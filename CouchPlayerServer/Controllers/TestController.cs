﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;

namespace CouchPlayerServer.Controllers
{
    public class TestController : Controller
    {
        public IActionResult Index2()
        {
            return View();
        }

        // Backup
        private IRepositoryWrapper mRepoWrapper;


        public TestController(IRepositoryWrapper repoWrapper)
        {
            mRepoWrapper = repoWrapper;
        }


        public IActionResult Index()
        {

            VideoElement tmpElem = new VideoElement();
            tmpElem.Id = 1;
            tmpElem.Title = "Test";

            tmpElem.IsFolderElement = false;
            tmpElem.FileFormat = "mp4";
            tmpElem.FileName = "test.mp4";
            tmpElem.Path = "C:\temp";
            tmpElem.FullPath = "C:\temp\test.mp4";
            tmpElem.Duration = new TimeSpan();
            tmpElem.Framerate = 60;
            tmpElem.ResWidth = 1920;
            tmpElem.ResHeight = 1080;


            mRepoWrapper.VideoElements.Create(tmpElem);
            mRepoWrapper.VideoElements.Save();

            var video = mRepoWrapper.VideoElements.FindAll();


            return View();
        }

    }
}